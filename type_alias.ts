type CarYear = number;
type CarType = string;
type CarModel = string; 

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const carYear: CarYear = 2001;
const carType: CarType = "Toyota";
const carModel: CarModel = "Corolla";

const car1: Car = {
    year: carYear,
    type: carType,
    model: carModel,
}

const car2: Car = {
    year: 1978,
    type: "Truck",
    model: "Tuktuk",
}

console.log(car1);
console.log(car2);